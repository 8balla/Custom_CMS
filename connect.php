<?php

define('DB_HOST', '149.255.58.10');
define('DB_USER', '');
define('DB_PASS', 'p@55w0rd');
define('DB_NAME', '');
define('DB_PREFIX', 'mc_');

define('DB_CHAR_SET', 'utf8');

define('DB_LOCALE', 'en_GB');

define('DB_TIMEZONE', '+00:00');

define('SECRET_KEY', '');

define('ENABLE_MYSQL_ERRORS', 1);
define('MYSQL_DEFAULT_ERROR', 'Database Error - Check Back Later');

?>
