<?php

define('DEF_OPEN_MENU_PANEL', 0);

define('OFF_CANVAS_PARENT_AS_CHILD', 1);

define('BUY_NOW_CODE_OPTION', 1);

define('HISTORY_PER_PAGE', 20);
define('SAVED_SEARCHES_PER_PAGE', 20);
define('WISHLIST_PER_PAGE', 20);
define('HOMEPAGE_PRODUCTS_LIMIT', 3);
define('ACC_DASH_LATEST_COUNT', 5);
define('VIEW_ALL_CATEGORY_DD', 'no');
define('VIEW_ALL_BRANDS_DD', 'no');
define('VIEW_ALL_SPECIALS_DD', 'no');
define('VIEW_ALL_SEARCH_DD', 'no');
define('VIEW_ALL_LATEST_DD', 'no');
define('VIEW_ALL_WISH_DD', 'no');
define('DOWNLOADS_FLUSH_BUFFER', 1);
define('BREADCRUMBS_SEPARATOR', ' <i class="fa fa-angle-right fa-fw"></i> ');
define('LEFT_MENU_BASKET_CHAR_LIMIT', 60);
define('COMPARISON_ORDER_BY', 'pName');
define('RELATED_ORDER_BY', 'pName');
define('BRANDS_ORDER_BY', 'name');
define('AUTO_SORT_PERSONALISATION_OPTIONS', 1);
define('PERSONALISATION_TEXT_RESTRICTION', 200);
define('ORDER_SPECIAL_OFFERS', 'pName');
define('CATEGORY_FILTER', 'title-az');
define('BRANDS_FILTER', 'title-az');
define('SEARCH_FILTER', 'title-az');
define('SPECIAL_OFFER_FILTER', 'title-az');
define('LATEST_PRODUCTS_FILTER', 'date-new');
define('WISH_PRODUCTS_FILTER', 'title-az');
define('SEARCH_ORDER_BY', 'pName');
define('DOWNLOADS_ORDER_BY', 'pName');
define('CHECKOUT_ZONE_ORDER_BY', 'zName');
define('CHECKOUT_ZONE_AREA_ORDER_BY', 'areaName');
define('ZONE_AREA_DELIMITER', ',');
define('DEFAULT_CHECKED_GIFT', '');
define('CONTACT_AUTO_RESPONDER', 1);
define('PROD_ENQUIRY_AUTO_RESPONDER', 1);
define('RSS_BUILD_DATE_FORMAT', date('r'));
define('AUTO_PARSE_LINE_BREAKS', 1);
define('EMAIL_PERSONALISATION_INCL', 1);
define('EMAIL_ATTRIBUTES_INCL', 1);
define('EMAIL_GIFT_FROM_TO_INCL', 1);
define('RESPONSE_REFRESH_TIME', 5);
define('RESPONSE_PAGE_REFRESHES', 12);
define('TAG_SEPARATOR', ', ');
define('NEWSLETTER_EMAIL_AUTO_RESPONDERS', 1);
define('PRICE_THOUSANDS_SEPARATORS', ',');
define('YOU_TUBE_EMBED_CODE','<iframe src="https://www.youtube.com/embed/{CODE}" style="border:0 !important" allowfullscreen></iframe>');
define('VIMEO_EMBED_CODE', '<iframe src="https://player.vimeo.com/video/{CODE}" style="border:0 !important" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>');
define('DAILY_MOTION_EMBED_CODE', '<iframe src="https://www.dailymotion.com/embed/video/{CODE}" style="border:0 !important" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>');
define('SOUNDCLOUD_EMBED_CODE','<iframe width="100%" height="166" scrolling="no" style="border:0 !important" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/{CODE}&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false"></iframe>');
define('MP3_EMBED_CODE','<audio controls><source src="{MP3}" type="audio/mpeg">Your browser does not support MP3 files via HTML5 audio tags.</audio>');
define('ENTRY_LOG_SKIP_USERS', '');
define('TRACKING_CODE_PREFIX', 'tr');
define('CURRENCY_CONVERTER_CRON_OUTPUT', 1);
define('PRICE_FORMAT_DECIMAL_PLACES', 2);
define('BACKUP_CRON_OUTPUT', 1);
define('POPS_CRON_OUTPUT', 1);
define('BACKUP_CRON_EMAILS', '');
define('MC_MB_BANNERS', 0);
define('MAIL_FROM_NAME_HEADER', '');
define('MAIL_FROM_EMAIL_HEADER', '');
define('SHIP_DEBUG_LOG', 0);
define('CHECKOUT_DEBUG_LOG', 0);
define('PRODUCTS_FOLDER', 'content/products');
define('VIDEO_FOLDER', 'content/video');
define('BANNER_FOLDER', '{theme}/images/banners');

?>
